import { useState } from "react";

function CustomerList({ customers, getCustomers }) {
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);

    async function handleDelete(event) {
        event.preventDefault();
        const customerUrl = `http://localhost:8090${event.target.value}`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            getCustomers();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    return (
        <>
            <h1>List of Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={customer.href}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number}</td>
                                <td>{customer.address}</td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={customer.href}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    )
}

export default CustomerList;
