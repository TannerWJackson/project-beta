import React, { useState} from 'react';

function TechnicianForm({ getTechnicians }) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };
        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoResponse = await fetch(technicianUrl, fetchOptions);
        if (autoResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');

            getTechnicians();
        }
    }
    function handleFirstNameChange(event) {
        const value=event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const value=event.target.value;
        setLastName(value);
    }

    function handleEmployeeIdChange(event) {
        const value=event.target.value;
        setEmployeeId(value);
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} value={employee_id} placeholder="employee id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
 export default TechnicianForm;
