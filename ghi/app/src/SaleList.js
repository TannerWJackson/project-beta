import { useState } from "react";

function SaleList({ sales, getSales }) {
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);

    async function handleDelete(event) {
        event.preventDefault();
        const saleUrl = `http://localhost:8090/api/sales/${event.target.value}/`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            getSales();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    return (
        <>
            <h1>List of Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={`${sale.id}+${sale.automobile}`}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                <td>{sale.customer}</td>
                                <td>{sale.automobile}</td>
                                <td>{`$${sale.price}`}</td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={sale.id}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    )
}

export default SaleList;
