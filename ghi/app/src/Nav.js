import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className='dropdown'>
              <button className='btn btn-outline-light dropdown-toggle' type='button' id="manufacturerMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </button>
              <ul className='dropdown-menu' aria-labelledby='manufacturerMenuButton'>
                <li>
                  <Link className='dropdown-item' to='/manufacturers'>
                    List of Manufacturers
                  </Link>
                  <Link className='dropdown-item' to='manufacturers/create'>
                    Create Manufacturer
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className='btn btn-outline-light dropdown-toggle' type='button' id="modelsMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Models
              </button>
              <ul className='dropdown-menu' aria-labelledby='modelsMenuButton'>
                <li>
                  <Link className='dropdown-item' to='/models'>
                    List of Vehicle Models
                  </Link>
                  <Link className='dropdown-item' to="/models/create">
                    Create Vehicle Model
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className='btn btn-outline-light dropdown-toggle' type='button' id="autosMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </button>
              <ul className='dropdown-menu' aria-labelledby='autosMenuButton'>
                <li>
                  <Link className='dropdown-item' to="/automobiles">
                    List of Automobiles
                  </Link>
                  <Link className='dropdown-item' to="automobiles/create">
                    Create Automobiles
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-outline-light dropdown-toggle" type="button" id="salesMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </button>
              <ul className="dropdown-menu" aria-labelledby="salesMenuButton">
                <li>
                  <Link className="dropdown-item" to="/sales">
                    List of Sales
                  </Link>
                  <Link className='dropdown-item' to="/sales/salesperson">
                    Sales by Salesperson
                  </Link>
                  <Link className='dropdown-item' to="/sales/create">
                    New Sale
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-outline-light dropdown-toggle" type="button" id="salespersonMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </button>
              <ul className="dropdown-menu" aria-labelledby="salespersonMenuButton">
                <li>
                  <Link className="dropdown-item" to="/salespeople">
                    List of Salespeople
                  </Link>
                  <Link className='dropdown-item' to="/salespeople/create">
                    New Salesperson
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-outline-light dropdown-toggle" type="button" id="customersMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </button>
              <ul className="dropdown-menu" aria-labelledby="customersMenuButton">
                <li>
                  <Link className="dropdown-item" to="/customers">
                    List of Customers
                  </Link>
                  <Link className='dropdown-item' to="/customers/create">
                    New Customer
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-outline-light dropdown-toggle" type="button" id="appointmentMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </button>
              <ul className="dropdown-menu" aria-labelledby="appointmentMenuButton">
                <li>
                  <Link className="dropdown-item" to="/appointments">
                    List of Appointments
                  </Link>
                  <Link className='dropdown-item' to="/appointments/history">
                    History of Appointments
                  </Link>
                  <Link className='dropdown-item' to="/appointments/create">
                    New Appointments
                  </Link>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-outline-light dropdown-toggle" type="button" id="techniciansMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </button>
              <ul className="dropdown-menu" aria-labelledby="techniciansMenuButton">
                <li>
                  <Link className="dropdown-item" to="/technicians">
                    List of Technicians
                  </Link>
                  <Link className='dropdown-item' to="/technicians/create">
                    New Technicians
                  </Link>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
