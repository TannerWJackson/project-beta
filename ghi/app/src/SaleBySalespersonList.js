import { useState } from "react";

function SaleBySalespersonList({salespeople}) {
    const [ sales, setSales ] = useState([]);
    const [ salespersonId, setSalespersonId ] = useState();
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);

    async function getSales() {
        const salesUrl = `http://localhost:8090/api/sales/salesperson/${salespersonId}/`;
        const response = await fetch(salesUrl);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    async function handleDelete(event) {
        event.preventDefault();
        const saleUrl = `http://localhost:8090/api/sales/${event.target.value}/`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            getSales();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleSalespersonIdChange(event) {
        const value=event.target.value;
        setSalespersonId(value);
    }

    function handleSubmit(event) {
        event.preventDefault();
        getSales();
    }

    return (
        <>
            <h1>List Sales by Salesperson</h1>
            <div className="container">
                <form onSubmit={handleSubmit}>
                    <div className="d-flex">
                        <select onChange={handleSalespersonIdChange} required name="salesperson" id="salesperson" className="form-select mr-1">
                            <option value="">Choose a salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.href} value={salesperson.id}>
                                        {`${salesperson.first_name} ${salesperson.last_name}`}
                                    </option>
                                )
                            })}
                        </select>
                        <button className="btn btn-primary">Select</button>
                    </div>
                </form>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={`${sale.id}+${sale.automobile}`}>
                                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                <td>{sale.customer}</td>
                                <td>{sale.automobile}</td>
                                <td>{`$${sale.price}`}</td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={sale.id}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    )
}

export default SaleBySalespersonList;
